# Recoding with the `rock` R package {#rock-package-recoding}

## Initial coding versus recoding

We use recoding to refer to the coding decisions and actions taken after the initial coding. In initial coding, codes are often attached to utterances using a text editor (such as Notepad++) or a graphical user interface (such as iROCK). These decisions are often based on categorization of utterances, and sometimes initial coding mostly concerns indexing, where the data is organized superficially, for example based on topic, instead of looking for the patterns that the researcher is interested in.

If the aim is to work in a transparent, reproducible manner, initial coding will often occur based on instructions in a coding book; if not decisions during initial coding will be based on implicit definitions, often under the assumption that those are shared by all involved researchers and sometimes even the relevant academic discipline.

After this first 'coding sweep', researchers often want to recode. When the initial coding consisted of indexing (i.e. organising the data based on the topic that was discussed), the coding necessary to address the research question won't even have started yet --- but even when it has, once it's possible to view all source fragments that were coded with a specific code, patterns often become apparent that were harder to spot before having seen the data grouped by code. In addition, the coding structure can often be improved to represent patterns in the data better.

The choices made during recoding


<!----------------------------------------------------------------------------->

## Deleting codes {#rock-package-recoding-deleting-codes}

