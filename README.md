# The Reproducible Open Coding Kit book

The rendered version of this Bookdown book is available at https://rockbook.org. The references are kept in the public Zotero group at https://www.zotero.org/groups/2385284/the_rock_book, and read automatically using the 'zotero_to_bibtex.R' script.
