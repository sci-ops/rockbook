---
editor_options: 
  markdown: 
    wrap: 72
---

Intro: so, in the Netherlands there's a kind of saying "organizing your
disagreement" (kind of), basically meaning that it's a good idea to make
sure you have people around who almost on principle argue against, well,
pretty much everything :-) The idea is that by engaging in those
discussions you're less likely to overlook stuff and that by unearthing
more arguments for all positions, whatever you end up with is better
supported. I feel like we have a healthy balance between this
(disagreeing and fleshing out those disagreements) and agreeing and
building cool stuff. So, again, thank you for that Szilvia :-)

# Szilvia, 2022-01-03

so... i thought about it and i think i would do this:

during study design, come up with relevant questions or answer the
following:

1)  How would you answer your RQs? Explain or justify you answers, which
    may be based on guesses, ideal scenarios, or previous research
    findings

2)  what findings would be surprising for you and why?

3)  what has to work/exist for your chosen methods to elicit the data
    you want?

4)  describe your positionality regarding your RQs and/or topic.
    Describe any prior contact or experience you have.

5)  Do you have any strong feelings about any aspect of the research
    topic? Do you have any intense memories from previous initiatives in
    the same topic? Does any aspect of the data induce any strong
    emotions?

6)  How do your research goals and your relevant personal or
    professional interests relate?

7)  Describe the relationship of the research team members. Describe the
    dynamics within the research group, especially detail the
    relationship and expertise of those who will be developing codes and
    performing coding.

answer these questions (and any others) at (at least) the following
points in the research process: 0) design phase / prereg 1) code
development and/or code finalization 2) after coding and/or after
initial analysis 3) at write-up

these can be called something like reflexivity-guiding questions, and
the answers can be documented in a reflexivity journal

# GJ, 2021-01-04

I think these are excellent points, and I think that in a chapter on
reflexivity and positionality these would serve very well to structure
the type of questions you can use to guide your reflection at different
stages of the process.

I see these as important parts of conducting qualitative (or, for that
matter, quantitative) research, so these should definitely be included.
Simultaneously, this does not fulfill the purpose of operationalizing
researchers' preconceptions and their representations of the world as
they are relevant to the data that will be (or were) collected (e.g.,
which categories are used to process the world, which things are
causally related to which other things, things of which categories are
members of other categories, etc).

Answering the questions listed above are very helpful to guide general
reflection, and they are very helpful to clarify how you're approaching
the study without imposing a specific structure on those reflections.
This gives people a great deal of freedom as to how they formulate their
reflections, how comprehensively they think about them, and how much
effort they make to make sure that what they document is accessible,
unambiguous, and clear to other researchers (or, for that matter, future
them). There are lots of reasons why this is desirable.

At the same time, this has drawbacks. One obvious drawback is exactly
the inverse of this freedom: researchers are not guided in how to
document their preconceptions in a comprehensive and unambiguous manner,
which will result in less transparency. A second drawback is that these
questions at best only touch peripherally on researchers'
representations of the world and how these shape the categorizations
that are sensible to them. A third drawback is that the freedom of the
format makes identifying manifestations of preconceptions in the code
structure that is developed during inductive coding very hard. It
requires a lot of subjective judgments on the part of the researchers
assessing the coding that involve attempting to infer what was in the
original researchers' minds from their answers to those questions and
comparing that with the codes that those researchers created.

By providing researchers with guidance to format their representations
and preconceptions into codes these drawbacks are addressed. This
provides a consistent operationalization that involves familiar skills
that researchers use during coding anyway: identifying the relevant
concepts and explicating those into descriptions, coding instructions,
and examples. An additional benefit is that the coding phase becomes
more straightforward: because you always start out with an initial code
set, the coding always comes down to comparing of data fragments to
existing codes. This creates a workflow where it is easy to implement
explicit justification of every aspect of the ultimate code structure:
either the codes were part of the initial code set, and so do not
reflect insights derived from the data, or the codes were added
inductively, and so way the the codes relate to the data and the
considerations playing a role when they were created (e.g. which
characteristics of those data fragments rendered them too dissimilar to
fit with existing codes) are documented (assuming users aim to exercise
transparency during coding).

During coding, then, a data fragment can then either be considered to
express nothing of interest to the study at hand; be considered an
obvious match with one or more existing codes, which are then attached;
be considered to not match a code perfectly but sufficiently to code it
regardless (i.e., assimilation), be considered cause to change the code
a bit (i.e., accomodation), or be considered cause to add a new code
that captures a concept expressed in that data fragment (omitting, for
the moment, decisions related to restructuring the code structure).

You expressed a concern with this approach, specifically that by
prompting people to reflect on their relevant representations and
preconceptions and guiding them to operationalize these in an initial
code set, this will make it less likely for them to identify new codes,
and more inclined to assimilate relevant expressions encountered in the
codable data fragments into codes in the initial code set. I do not
believe this is a risk because of what are apparently, now I tried to
delineate into separate points, five reasons :-)

First, people are bad at coding, because people are basically pattern
recognition machines. So people perceive lots of patterns that are hard
to see for e.g., computers -- but people also frequently perceive
patterns that are not present; interpret patterns differently as a
consequence of their existing representations of the world; and fail to
perceive existing patterns because it can't be easily processed using
their existing representations. People are nothing like blank slates
that have a 'neutral state' from which they can start coding; and so,
there is no neutral, 'immaculate' state that can be disturbed by a
priori reflection and operationalization of the products of that
reflection into initial codes.

--- Aaand I just got boostered, finally! 🎉

Anyway 😬 Second, making specific categories salient brings them into
short-term memory briefly, but this effect is transient and this
information doesn't "block" anything else that is perceived from
entering short-term memory. In addition, people talk to other people all
the time: read articles, have discussions, think about topics, consume
(social) media: there are practically infinite stimuli that result in
enhanced category salience. As such, to set as a goal to insulate people
from selective category salience seems unfeasible. Instead, the best we
can hope for is to enable transparency as to the representations that
shape the interpretation of data fragments.

Third, there is no reason to believe that people are so easily
influenced, and ample evidence that they are not. The priming studies
that fail to replicate are a salient example, but psychological research
shows that people are quite, well, stubborn, and that stubbornness is
quite stable (from a complex systems perspective: hard to push people to
a different equilibrium). This is also visible in behavior change
research: getting people to behave differently is very hard
(unfortunately). And this extends to coding data. This is a complex
task, requiring considerably cognitive processing and reflection during
that processing. Making some categories more salient, or explicating
them into an initial code set and providing that code set as a starting
point for the coding activity, doesn't make people perform less
processing, especially given that their aim is inductive coding. That
skill (to accurately weigh characteristics of what is expressed in a
data fragment against the existing codes, coding instructions, etc, and
then decide to not code, code, assimilate, accomodate, restructure, etc)
is a skill researchers who code need to possess and put into practice
during coding anyway: they don't start taking a back seat once there is
already one or more codes present in the coding structure. Otherwise,
coding would be unreliable from the moment one inductive code was
created (or two, or three, etc, wherever you put the threshold).

Of course, it is possible that for some reason the act of coding data is
exceptional and that for coding qualitative data, there *is* a high risk
of biasing coding decisions towards assimilation if researchers think
long and hard about their views before they start coding. However, even
if that is the case we get to the fourth reason why I don't think that
in inductive coding scenarios (because all this doesn't apply when
people start off with a code structure anyway), getting people to
operationalize their a priori reflections in an initial code set is not
problematic. That is that *if* comprehensive a priori reflection
influences the coding process, that will also happen when the a priori
reflection phase doesn't have an operational component. Compared to the
cognitive elaboration that takes place during comprehensive reflection
anyway, operationalization doesn't add a lot: it just standardizes the
process. If comprehensive a priori reflection has an influence anyway,
only very cursory reflection could avoid such influence - which I assume
we both agree is not the recommendation we want to give. Of course, if
people don't operationalize their a priori preconceptions and pertinent
representations in advance, such influence, if it occurs, will be very
hard to detect. Meta-scientists will have a hard time answering the
underlying empirical question; that will require a lot of interpretation
of how what is expressed in people's journals would translate into an
initial code set. A task the original researchers are much beter placed
to perform, at a time when their interpretations aren't yet influence by
the data.

Finally, fifth, assuming that not the comprehensive a priori reflection,
but the operationalization of that reflection in codes *is* the 'active
ingredient' that somehow has a considerable effect on the coding
process. I'd argue that even in that scenario, starting with an initial
code set is less bad than not doing so, because the value of making the
bias as transparent as possible is higher than the cost of that bias
increasing from an unknown amount to a higher unknown amount. "Better
the devil you know," or something like that.

I hope this helps to explain why I'm so confident that using an initial
code set even for inductive coding, counter-intuitive at may be. I guess
it boils down to me rejecting the premise underlying 'inductive coding':
the idea that it's possible to capture what's in the data regardless of
your starting perspective. Given that I believe that your starting
representations and preconceptions will inevitably bias your coding, I
think that structuring the process, designing it to optimize
transparency, creating clear opportunities for explicit justification,
and making it as easy as possible to estimate the degree of bias in a
given project is the best we can hope for. Since you generally can't
really learn much from a single study anyway (too many problems
plagueing sampling, data collection, analyses, etc etc, so any results
have high degrees of uncertainty), facilitating the determination of
risk of bias in studies is pretty vital.

But, I might be wrong about any or all of this of course :-) In any
case, if you read this far, thank you for that already :-)

And - welcome back in the US :-)

# Szilvi, 2022-01-07

Thanks...although, technically, you can only say "welcome back" if you
are where the person goes back to. I think. But, thanks for the gesture.
Also, happy you're boosted, finally.

You make good points. Seriously...I was almost totally convinced, but
then I thought about it. :) Let me skip to the end, tho: yes, I agree
this could be an optional phase within the code development process of a
qual study that uses inductive coding or uses inducitvely created codes
to deductively code their data. Having said that, I do want to voice
some concerns.

Your reasoning is crucially based on your assertion that "priming
studies don't replicate" and thus dismiss priming in general (the
no-blank-slate argument, I will get back to). Concerning priming,
unfortunately I will have to take your word for: 1) priming studies
indeed don't replicate, and 2) they don't replicate because priming
isn't a thing (and not bc of sampling issues, differing populations,
non-open/incomplete procedure descriptions, etc.). So, taking your word
for this is quite a leap, I think. I would want to see how many didn't
replicate and for what reasons and only then draw conclusions about
whether or not priming has an effect. Since I am not going to embark on
this journey, I will - as mentioned - take your word on all this.

I agree when you say we are not developing codes from a blank slate
condition, our assumptions/preconceptions always influence how we
interpret the data, I honestly doubt *anyone* would argue with you
there. AND I also agree that thinking about the reflexivity questions I
outlined above bring you close to the kind of "operationalization" you
outline in your "initial coding" idea. But I do think there is a
difference between the two in intensity and effect. Thinking about your
preconceptions *in context* and related to each other and the RQs is one
thing, but "solidifying" them into codes is another. As you say, people
can get super subborn, and in this case, they can easily want to cling
to the codes developed in the initial coding phase. I know from
experience, it is very hard to "let go" of codes...I see it in others as
well. Once you reify an idea into a construct, theme, or code, it gets
very difficult to manipulate (merge, delete, split, etc.). While
answering the questions I have above keeps your ideas in a fluid state
but manages to bring them to the foreground enough for you to be able to
reflect on and use them later on in your process to continue practicing
reflexivity, creating codes solidifies them too much in your future
thinking. And here enters your point about stubbornness: the more I
believe something is a *thing*, the less likely I am to cognitively
depart from it.

I also agree when you say it's easy to cheat with the reflexivity
questions and difficult to "evaluate" for openness or completeness :)
well...it's just as easy to perform initial coding in a poor manner,
giving it close to no thought. Very easy. But I do see your point in 1)
it's difficult for researchers to use and make sense of the "qual data"
from their reflexivity questions, 2) it's even more difficult to make
sense of these for other researchers and it's practically useless for
meta-science, 3) there should be a way to "distill" whatever is in the
answers so that it matches or caters to the rest of the code development
process.

Thus, I'll concede that initial coding is not completely absurd. But I
do still have issues with it. Namely: Once you've developed these codes,
and you so happen to keep, say, 3 out of 5 initial codes and they are
retained in the final (greater) code structure, what then? You mentioned
to be "skeptical" with these codes. Why? I may have pinpointed apt codes
in the initial coding phase because: -I got lucky -I had conducted
several studies prior and am familiar with similar data -I primed myself
:) but can justify keeping them -etc So, how should I know which of
these senarios is happening? And of similar import: how do I "correct"
it? In the above example, I feel 3 codes are justifiably in the final
code structure. Should they just be removed? Also, codes usually exhibit
some kind of relationship to one-another. Once I remove or change a code
because of reflexivity-reasons (what are we calling this, by the way?),
then that will potentially have an effect on (part of or) the entire
code structure. How should I deal with that? And when, in my code
development or coding process, should I deal with that?

OK, so, these are my preliminary thoughts...I'm not sure how I would
decide if you'd ask me "should there be such a thing as 'initial
coding'", honestly...I'm uncertain. I'm also uncertain that this depth
of methodology has its place in the ROCK Book. We either say 1) we will
expand the Rock standard to the entire qual research process (which
entails a lot more thinking and discussing), or 2) we start compiling
these ideas in a book on qual methodology (which also involves more
thinking and discussing) but at least in the end, no matter which we
choose, the world would have a *good* qual methods book. Also, in the
second option, I think there is room to discuss disagreement...I'm
pretty sure it would be useful for people to see (at least our 2)
different perspectives on some *minor* questions like this :D
